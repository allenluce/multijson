// Write JSON objects from input to multiple files
//
// Allen Luce, aluce@adonnetwork.com
//
// v0.2
//
// This program expects input to consist of one or more JSON objects in a format like this:
//
// {
//   "property1": "value1",
//   "property2": "value2"
//   ...
// }
// {
//   "property1": "value3",
//   "property2": "value4"
//   ...
// }
// ...
//
// Run like this:
//
// multijson -k property1 < file.json
//
// It will create two separate files, "value1" and "value3", each
// containing the object that was associated with that property value.
//
// Given a -d option, the property that the filename is derived from will
// be removed from the JSON object before writing.
//
// An -x option will add an extension to the file that's being written:
//
// multijson -k property1 -x .json < file.json
//

#include <iostream>
#include <fstream>
#include <unordered_map>
#include <string>
#include <algorithm>
#include "rapidjson/reader.h"
#include "rapidjson/rapidjson.h"
#include "rapidjson/writer.h"
#include "rapidjson/filestream.h"
#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"

using namespace rapidjson;
using namespace std;

class MultiJsonWriter {
  unordered_map <string, ofstream*> files;
  bool delete_field = false; // Delete the field we're keying off?
  string add_extension = ""; // What extension to add.

  void write_document(string file_template, Document *document) {
    StringBuffer buffer;
    Writer<StringBuffer> writer(buffer);
    document->Accept(writer);
    string str = buffer.GetString();
    if (!files[file_template])
      files[file_template] = new ofstream(file_template + add_extension, ios::out | ios::trunc);
    *files[file_template] << str << endl;
  }

public:
  void set_delete_field(bool value) {
    delete_field = value;
  }

  void set_add_extension(string value) {
    add_extension = value;
  }

  int parse_input(const char *key_name) {
    Reader reader;
    FileStream is(stdin);
    Document document;
    StringBuffer buffer;
    Writer<StringBuffer> writer(buffer);
    bool more;

    do {
      size_t object_begin = is.Tell();

      if ((more = !reader.Parse<0>(is, writer))) {
        if ((string) reader.GetParseError() != "Nothing should follow the root object or array.") {
          cerr << endl << "Error at pos " << reader.GetErrorOffset() << ": " << reader.GetParseError() << endl;
          return 1;
        }
      }

      StringStream s(buffer.GetString());
      document.ParseStream<0>(s);
      if (!document[key_name].GetType()) {
        cerr << "Cannot find key " << key_name << " at " << object_begin << endl;
        return 1;
      }
      auto filename = document[key_name].GetString();
      if (delete_field)
        document.RemoveMember(key_name);

      write_document(filename, &document);
      buffer.Clear();
    } while (more);

    return 0;
  }
};


bool is_opt_set(int argc, char *argv[], string option) {
  char **s = find(argv, argv+argc, option);
  return s != argv+argc;
}

char *get_opt(int argc, char *argv[], string option) {
  char **s = find(argv, argv+argc, option);
  if (s != argv+argc && ++s != argv+argc)
    return *s;
  return 0;
}

int main(int argc, char* argv[]) {
  MultiJsonWriter m;
  const char *key_name = "doc_type";

  if (is_opt_set(argc, argv, "-k"))
    key_name = get_opt(argc, argv, "-k");

  m.set_delete_field(is_opt_set(argc, argv, "-d"));

  if (is_opt_set(argc, argv, "-x"))
    m.set_add_extension(get_opt(argc, argv, "-x"));

  return m.parse_input(key_name);
}
