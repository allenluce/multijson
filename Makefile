CXX=g++
RM=rm -f

SRCS=multijson.cc
OBJS=$(subst .cc,.o,$(SRCS))
CPPFLAGS=-g -Irapidjson/include --std=c++11
LDFLAGS=-g
LDLIBS=

all: multijson

multijson: $(OBJS)
	$(CXX) $(LDFLAGS) -o multijson $(OBJS) $(LDLIBS)

multijson.o: multijson.cc

clean:
	$(RM) $(OBJS)

distclean: clean
	$(RM) multijson
